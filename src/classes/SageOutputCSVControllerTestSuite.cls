/**
* @author Denis Borisevich
* @date 11/08/2016
* @description 
*/

@isTest
private with sharing class SageOutputCSVControllerTestSuite {
	
	@TestSetup
	private static void init() {
		ItineraryTestHelper itineraryTest = new ItineraryTestHelper();
		itineraryTest.itinerary.CurrencyIsoCode = 'SEK';
		update itineraryTest.itinerary;
		
		Account supplier = [select Id from Account limit 1];
		KaptioTravel__Brand__c brand = [select Id from KaptioTravel__Brand__c limit 1];

		KaptioTravel__SupplierInvoice__c si = new KaptioTravel__SupplierInvoice__c(
			KaptioTravel__Account__c = supplier.Id,
			KaptioTravel__Brand__c = brand.Id,
			KaptioTravel__Amount__c = 100);
		insert si;

		KaptioTravel__SupplierInvoiceBooking__c sib = new KaptioTravel__SupplierInvoiceBooking__c(
			KaptioTravel__Supplier__c = supplier.Id);
		insert sib;
		
		KaptioTravel__Itinerary_Item__c itineraryItem = [SELECT ID from KaptioTravel__Itinerary_Item__c limit 1];
		itineraryItem.KaptioTravel__SupplierBuyRate__c = 2;
		update itineraryItem;

		KaptioTravel__SupplierInvoiceLine__c sil = new KaptioTravel__SupplierInvoiceLine__c(
			KaptioTravel__Supplier__c = supplier.Id,
			KaptioTravel__PaymentRuleType__c = '0',
			KaptioTravel__SupplierInvoiceBooking__c = sib.Id,
			KaptioTravel__Itinerary__c = itineraryTest.itinerary.Id,
			KaptioTravel__ItineraryItem__c = itineraryItem.Id
			);
		insert sil;

		KaptioTravel__SupplierInvoiceAllocation__c allocation = new KaptioTravel__SupplierInvoiceAllocation__c(
			KaptioTravel__SupplierInvoice__c = si.Id,
			KaptioTravel__SupplierInvoiceLine__c = sil.Id,
			KaptioTravel__InvoiceAmount__c = 95
		);
		insert allocation;
	}
	
	@isTest 
	private static void test() {        
		KaptioTravel__SupplierInvoice__c supInvoice = [SELECT ID FROM KaptioTravel__SupplierInvoice__c limit 1];
		Test.setCurrentPageReference(new PageReference('Page.SageOutputCSV'));
		System.currentPageReference().getParameters().put('ids', supInvoice.Id);
		SageOutputCSVController outputCSV = new SageOutputCSVController();
	}
}