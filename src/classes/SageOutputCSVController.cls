public class SageOutputCSVController {
	public List<OutputStructure> outputStructList { get; private set; }
	public Boolean isDifferentISOCode { get; private set; }
	
	public SageOutputCSVController() {
		outputStructList = new List<OutputStructure>();
		isDifferentISOCode = false;
		Set<Id> ids = getIdSetFromCommaSeperatedString(ApexPages.currentPage().getParameters().get('ids'));
		List<KaptioTravel__SupplierInvoice__c> supplierInvoices = (List<KaptioTravel__SupplierInvoice__c>)DataBase.query('Select KaptioTravel__Account__r.Supplier_Sage_Code__c,' + String.join(getAllFields(KaptioTravel__SupplierInvoice__c.SObjectType), ',') + ', (SELECT KaptioTravel__InvoiceAmountBCY__c, KaptioTravel__SupplierInvoiceLine__r.KaptioTravel__Itinerary__r.CurrencyIsoCode, KaptioTravel__SupplierInvoiceLine__r.KaptioTravel__Itinerary__r.KaptioTravel__Itinerary_Amount_BCY__c FROM Kaptiotravel__Supplier_Invoice_Allocation__r) FROM KaptioTravel__SupplierInvoice__c where id in :ids');
		for (KaptioTravel__SupplierInvoice__c curSupInvoice : supplierInvoices) {
			OutputStructure newStruct = new OutputStructure(curSupInvoice);
			isDifferentISOCode = isDifferentISOCode | newStruct.otherFields.size() > 1;
			if (newStruct.otherFields.size() < 2) {
				newStruct.otherFields.add(new String[]{'','',''});
			}
			outputStructList.add(newStruct);
		}
	}
	
	private static Set<Id> getIdSetFromCommaSeperatedString(String ids) {
		Set<Id> output = new Set<Id>();
		if( ids.contains(',')) {
			for(String s : ids.split(',')) {
				output.add(Id.valueOf(s));
			}
		} else {
			output.add(ids);
		}
		return output;
	}
	
	private static List<String> getAllFields(Schema.SObjectType p_sobjectType) {

		List<String> writableFields = new List<String>();
		List<Schema.SObjectField> allSobjectFields = p_sobjectType.getDescribe().fields.getMap().values();

		for(Schema.SObjectField field : allSobjectFields) {
			writableFields.add(field.getDescribe().getName());
		}
		return writableFields;
	}

	class OutputStructure {
		public KaptioTravel__SupplierInvoice__c supInv { get; private set; }
		public List<List<String>> otherFields { get; private set; }
		
		public OutputStructure(KaptioTravel__SupplierInvoice__c supplierInvoice) {
			this.supInv = supplierInvoice;
			this.otherFields = new List<List<String>>();
			List<KaptioTravel__SupplierInvoiceAllocation__c> allocations = supplierInvoice.getSObjects('Kaptiotravel__Supplier_Invoice_Allocation__r');
			Decimal DKKAmount = 0;
			Decimal SEKAmount = 0;
			for (KaptioTravel__SupplierInvoiceAllocation__c allocation :allocations) {
				String isoCode = allocation.KaptioTravel__SupplierInvoiceLine__r.KaptioTravel__Itinerary__r.CurrencyIsoCode;
				if (isoCode == 'DKK') {
					DKKAmount += allocation.KaptioTravel__InvoiceAmountBCY__c;
				} else if (isoCode == 'SEK') {
					SEKAmount += allocation.KaptioTravel__InvoiceAmountBCY__c;
				}
			}
			String sageCode = supplierInvoice.KaptioTravel__Account__r.Supplier_Sage_Code__c;
			if (sageCode == null) {
				sageCode = '#MISSING_CODE#';
			}
			if (DKKAmount != 0) {
				List<String> dkkFields = new List<String>();
				dkkFields.add(String.valueOf(DKKAmount));
				dkkFields.add('5000');
				dkkFields.add('PI/' + sageCode);
				otherFields.add(dkkFields);
			} 
			if (SEKAmount != 0) {
				List<String> sekFields = new List<String>();
				sekFields.add(String.valueOf(SEKAmount));
				sekFields.add('5001');
				sekFields.add('PI/' + sageCode);
				otherFields.add(sekFields);
			}
		}
	}
}